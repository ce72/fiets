package ce.info.fiets.domain.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ArticleTest {

  private static final Article.ArticleBuilder WIKIPEDIA_ARTICLE_BUILDER = Article.builder()
      .feedTitle("feedTitle")
      .title("title")
      .htmlContent("content")
      .contentLink("url");

  @ParameterizedTest
  @MethodSource("testCaseProvider")
  void testExpiration(ZonedDateTime lastCheckDate, ZonedDateTime now, LocalTime targetTime, boolean isExpired) {
    Article article = WIKIPEDIA_ARTICLE_BUILDER
        .lastCheckDate(lastCheckDate.toInstant())
        .build();
    assertThat(article.isExpired(now, targetTime)).isEqualTo(isExpired);
  }

  private static Stream<Arguments> testCaseProvider() {
    // lastCheckDate, now, time, isExpired
    return Stream.of(
        Arguments.of("2020-04-11T11:00Z", "2020-04-12T12:00Z", "15:00", true), // older than 24 h
        Arguments.of("2020-04-11T13:00Z", "2020-04-12T12:00Z", "15:00", true), // no update from yesterday 15:00
        Arguments.of("2020-04-11T16:00Z", "2020-04-12T12:00Z", "15:00", false), // update from yesterday still valid
        Arguments.of("2020-04-12T11:00Z", "2020-04-12T12:00Z", "15:00", false), // update from today still valid
        Arguments.of("2020-04-12T14:00Z", "2020-04-12T12:00Z", "15:00", false), // update from today still valid
        Arguments.of("2020-04-11T16:00Z", "2020-04-12T16:00Z", "15:00", true), // update from yesterday, invalid today
        Arguments.of("2020-04-12T14:00Z", "2020-04-12T16:00Z", "15:00", true), // update from today, invalid today
        Arguments.of("2020-04-12T16:00Z", "2020-04-12T17:00Z", "15:00", false), // update from today, valid today
        Arguments.of("2020-04-12T16:00Z", "2020-04-13T06:00Z", "15:00", false) // update from today, valid tomorrow
    );
  }

}
