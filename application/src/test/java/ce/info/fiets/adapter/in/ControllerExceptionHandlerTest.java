package ce.info.fiets.adapter.in;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.Map;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ControllerExceptionHandlerTest {
  public static final String TEST_MESSAGE = "for test";
  private ControllerExceptionHandler cut;

  @BeforeEach
  void setUp() {
    cut = new ControllerExceptionHandler();
  }

  @Nested
  class GivenIllegalArgumentException {
    private final IllegalArgumentException exception = new IllegalArgumentException(TEST_MESSAGE);

    @Test
    void thenReturnBadRequest() {
      ResponseEntity<Map<String, String>> response = cut.toResponse(exception);
      assertThat(response.getStatusCode(), is(equalTo(HttpStatus.BAD_REQUEST)));
    }

    @Test
    void thenReturnMessage() {
      ResponseEntity<Map<String, String>> response = cut.toResponse(exception);
      assertThat(Objects.requireNonNull(response.getBody()).get("message"),
          is(equalTo(TEST_MESSAGE)));
    }
  }

  @Nested
  class GivenIllegalStateException {
    private final IllegalStateException exception = new IllegalStateException(TEST_MESSAGE);

    @Test
    void thenReturnInternalServerError() {
      ResponseEntity<Map<String, String>> response = cut.toErrorResponse(exception);
      assertThat(response.getStatusCode(), is(equalTo(HttpStatus.INTERNAL_SERVER_ERROR)));
    }

    @Test
    void thenReturnMessage() {
      ResponseEntity<Map<String, String>> response = cut.toErrorResponse(exception);
      assertThat(Objects.requireNonNull(response.getBody()).get("message"),
          is(equalTo("java.lang.IllegalStateException: " + TEST_MESSAGE)));
    }
  }
}
