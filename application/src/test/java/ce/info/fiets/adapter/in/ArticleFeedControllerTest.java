package ce.info.fiets.adapter.in;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import ce.info.fiets.domain.model.Article;
import ce.info.fiets.domain.service.ArticleSource;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@ComponentScan(basePackages = {"ce.info.fiets.controller"})
class ArticleFeedControllerTest {
  private static final LocalTime DEFAULT_UPDATE_TIME = LocalTime.of(5, 0);
  private static final int DEFAULT_DEPTH = 5;
  private static final Instant ARTICLE_UPDATE_TIME = LocalDateTime.of(2020, 5, 29, 8, 53, 0, 0)
      .toInstant(ZoneOffset.UTC);
  private static final long ARTICLE_UPDATE_TIME_MILLIS = Date.from(ARTICLE_UPDATE_TIME).getTime();

  private static final Article WIKIPEDIA_ARTICLE = Article.builder()
      .feedTitle("de/Philosophie - daily wikipedia article")
      .lastUpdate(ARTICLE_UPDATE_TIME)
      .title("Kant")
      .content("content")
      .htmlContent("Kant was a blabla")
      .contentLink("https://de.wikipedia.org/Kant")
      .build();

  private static final Article CNN_ARTICLE = Article.builder()
      .feedTitle("http://www.cnn.com - daily article")
      .lastUpdate(ARTICLE_UPDATE_TIME)
      .title("CNN")
      .content("content")
      .htmlContent("This is new")
      .contentLink("http://www.cnn.com")
      .build();

  @MockitoBean
  private ArticleSource articleSource;

  // not @Autowired and not autoconfigured to be able to test @Validated
  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext context;

  @Autowired
  private ArticleFeedController cut;

  @BeforeEach
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context)
        .build();
  }

  @Nested
  class WhenGetSameArticle {
    @Test
    void shouldReturnSameArticleWhenAskedWithUpdateTime() throws Exception {
      when(articleSource.sameArticleFrom("www.cnn.com", LocalTime.of(13, 0), true, false))
          .thenReturn(CNN_ARTICLE);

      mockMvc.perform(
              get("/api/article?updateTime=13:00&allowScripting=true&contentUrl=www.cnn.com")
                  .accept(MediaType.APPLICATION_ATOM_XML))
          .andExpect(status().isOk())
          .andExpect(content().contentType("application/atom+xml"))
          .andExpect(xpath("feed/title").string("http://www.cnn.com - daily article"))
          .andExpect(xpath("//feed/entry[1]/id")
              .string(containsString("" + ARTICLE_UPDATE_TIME_MILLIS)))
          .andExpect(xpath("//feed/entry[1]/title").string("CNN"))
          .andExpect(content().string(containsString("This is new")));
    }

    @Test
    void shouldFailOnBadRequest() throws Exception {
      mockMvc.perform(
              get("/api/article?updateTime=43:00&contentUrl=" + "x".repeat(257))
                  .accept(MediaType.APPLICATION_ATOM_XML))
          .andExpect(status().isBadRequest())
          .andExpect(
              content().string(containsString(
                  "getArticle.contentUrl: size must be between 5 and 256")))
          .andExpect(content()
              .string(containsString(
                  "getArticle.updateTime: must match ")));
      verifyNoInteractions(articleSource);
    }
  }

  @Nested
  class WhenGetProxy {
    @Test
    void shouldReturnSameArticle() throws Exception {
      when(articleSource.alwaysDownload("www.cnn.com"))
          .thenReturn(CNN_ARTICLE);

      mockMvc.perform(
              get("/api/proxy?url=www.cnn.com"))
          .andExpect(status().isOk())
          .andExpect(content().string(containsString("content")));
    }
  }

  @Nested
  class WhenGetRandomLink {
    @Test
    void shouldReturnRandomLinkWhenAskedWithDefaults() throws Exception {
      when(articleSource.randomLinkFrom("www.cnn.com", "", DEFAULT_UPDATE_TIME))
          .thenReturn(CNN_ARTICLE);

      mockMvc.perform(
              get("/api/linkFrom?url=www.cnn.com")
                  .accept(MediaType.APPLICATION_ATOM_XML))
          .andExpect(status().isOk())
          .andExpect(content().contentType("application/atom+xml"))
          .andExpect(xpath("feed/title").string("http://www.cnn.com - daily article"))
          .andExpect(xpath("//feed/entry[1]/id")
              .string(containsString("" + ARTICLE_UPDATE_TIME_MILLIS)))
          .andExpect(xpath("//feed/entry[1]/title").string("CNN"))
          .andExpect(content().string(containsString("This is new")));
    }

    @Test
    void shouldReturnRandomLinkWhenIdAndUpdateTime() throws Exception {
      when(articleSource.randomLinkFrom("www.cnn.com", "body_content", LocalTime.of(15, 0)))
          .thenReturn(CNN_ARTICLE);

      mockMvc.perform(
              get("/api/linkFrom?updateTime=15:00&cssid=\"body_content\"&url=www.cnn.com")
                  .accept(MediaType.APPLICATION_ATOM_XML))
          .andExpect(status().isOk())
          .andExpect(content().string(containsString("This is new")));
    }

    @Test
    void shouldFailOnBadRequest() throws Exception {
      mockMvc.perform(
              get("/api/linkFrom?updateTime=43:00&url=x")
                  .accept(MediaType.APPLICATION_ATOM_XML))
          .andExpect(status().isBadRequest())
          .andExpect(content().string(containsString(
              "getLinkFrom.url: size must be between 5 and 256")))
          .andExpect(content().string(containsString(
              "getLinkFrom.updateTime: must match")));
      verifyNoInteractions(articleSource);
    }
  }

  @Nested
  class WhenGetWikipediaArticle {
    @Test
    void shouldReturnWhenAskedWithUpdateTime() throws Exception {
      when(articleSource.randomWikipediaArticleFor("de", "Philosophie", LocalTime.of(13, 0), 4))
          .thenReturn(WIKIPEDIA_ARTICLE);

      mockMvc.perform(
              get("/api/wikipedia/de/Philosophie?updateTime=13:00&depth=4")
                  .accept(MediaType.APPLICATION_ATOM_XML))
          .andExpect(status().isOk())
          .andExpect(header().string("Content-Type", "application/atom+xml"))
          .andExpect(xpath("//feed/title")
              .string("de/Philosophie - daily wikipedia article"))
          .andExpect(xpath("//feed/entry[1]/id")
              .string(containsString("" + ARTICLE_UPDATE_TIME_MILLIS)))
          .andExpect(xpath("//feed/entry[1]/title").string("Kant"))
          .andExpect(
              content()
                  .string(containsString(
                      "<title>de/Philosophie - daily wikipedia article</title>")));
    }

    @Test
    void shouldReturnWithDefaultValues() throws Exception {
      when(articleSource.randomWikipediaArticleFor("de", "Philosophie", DEFAULT_UPDATE_TIME,
          DEFAULT_DEPTH))
          .thenReturn(WIKIPEDIA_ARTICLE);

      mockMvc.perform(
              get("/api/wikipedia/de/Philosophie")
                  .accept(MediaType.APPLICATION_ATOM_XML))
          .andExpect(status().isOk())
          .andExpect(xpath("//feed/entry[1]/id")
              .string(containsString("" + ARTICLE_UPDATE_TIME_MILLIS)))
          .andExpect(
              content()
                  .string(containsString(
                      "<title>de/Philosophie - daily wikipedia article</title>")));
    }

    @Test
    void shouldFailOnBadRequest() throws Exception {
      mockMvc.perform(
              get("/api/wikipedia/x12345/y")
                  .accept(MediaType.APPLICATION_ATOM_XML))
          .andExpect(status().isBadRequest())
          .andExpect(content().string(containsString(
              "getWikipedia.language: size must be between 1 and 5")));
      verifyNoInteractions(articleSource);
    }
  }

  @Nested
  class WhenReset {
    @Test
    void shouldResetRepository() throws Exception {
      mockMvc.perform(
              post("/api/reset")
                  .accept(MediaType.APPLICATION_ATOM_XML))
          .andExpect(status().isOk());
      verify(articleSource).remove();
    }
  }
}
