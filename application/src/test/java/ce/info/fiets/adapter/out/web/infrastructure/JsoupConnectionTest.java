package ce.info.fiets.adapter.out.web.infrastructure;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class JsoupConnectionTest {
  private JsoupConnection cut;

  @BeforeEach
  void setUp() {
    cut = new JsoupConnection();
  }

  @Test
  void doesGetRealDocument() throws IOException {
    Document document = cut.get("http://de.wikipedia.org/wiki/Ontologie");
    assertThat(document).isNotNull();
    assertThat(document.title()).contains("Ontologie – Wikipedia");
  }

  @Test
  @Disabled
  void doesGetRealDocument2() throws IOException {
    Document document = cut.get("https://klimastation.frankfurt.de/export?exportFormat=csv&timezone=mesz&dataSource=klimareferat&sensor=A84041BD41827CEB&start=2025-02-15T00%3A00&end=2025-02-15T09%3A11");
    assertThat(document).isNotNull();
    assertThat(document.body().html()).isNotEmpty();
  }

}
