package ce.info.fiets.adapter.out.web.infrastructure;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.BDDMockito.given;

import ce.info.fiets.domain.model.RemoteException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HttpSourceTest {

  private static final String URL = "http://www.google.de";

  @Mock
  private JsoupConnection jsoupConnection;

  @Mock
  private Document document;

  @InjectMocks
  private HttpSource cut;

  @Test
  void doesGetDocumentFromJsoupConnection() throws IOException {
    given(jsoupConnection.get(URL)).willReturn(document);

    Document actual = cut.getFrom(URL);

    assertThat(actual).isEqualTo(document);
  }

  @Test
  void givenTimeoutThenThrow() throws IOException {
    given(jsoupConnection.get(URL)).willThrow(new SocketTimeoutException("cause"));

    assertThatExceptionOfType(RemoteException.class)
        .isThrownBy(() -> cut.getFrom(URL))
        .withMessage("Timeout while getting url http://www.google.de: java.net.SocketTimeoutException: cause");
  }

  @Test
  void givenHttpStatusThenThrow() throws IOException {
    given(jsoupConnection.get(URL)).willThrow(new HttpStatusException("cause", 500, URL));

    assertThatExceptionOfType(RemoteException.class)
        .isThrownBy(() -> cut.getFrom(URL))
        .withMessage("HTTP status 500 while getting url http://www.google.de");
  }

  @Test
  void givenMalformedUrlThenThrow() throws IOException {
    given(jsoupConnection.get(URL)).willThrow(new MalformedURLException("cause"));

    assertThatExceptionOfType(IllegalArgumentException.class)
        .isThrownBy(() -> cut.getFrom(URL))
        .withMessage("Malformed url while getting url http://www.google.de: java.net.MalformedURLException: cause");
  }

  @Test
  void givenIoExceptionThenThrow() throws IOException {
    IOException ioException = new IOException("cause");
    given(jsoupConnection.get(URL)).willThrow(ioException);

    assertThatExceptionOfType(RemoteException.class)
        .isThrownBy(() -> cut.getFrom(URL))
        .withMessage("Failed to get url http://www.google.de: java.io.IOException: cause")
        .withCause(ioException);
  }

}
