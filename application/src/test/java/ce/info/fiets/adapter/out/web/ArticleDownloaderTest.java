package ce.info.fiets.adapter.out.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.startsWith;
import static org.mockito.BDDMockito.given;

import ce.info.fiets.adapter.out.web.infrastructure.HttpSource;
import ce.info.fiets.adapter.out.web.infrastructure.JsoupConnection;
import ce.info.fiets.domain.model.Article;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;

@ExtendWith(MockitoExtension.class)
class ArticleDownloaderTest {
  private static final String BASE_URL = "http://myUrl";

  private static final Document linkList;

  static {
    linkList = Jsoup.parse(resourceAsString("test.html"));
    linkList.setBaseUri(BASE_URL);
  }

  @Mock
  private HttpSource httpSource;

  @InjectMocks
  private ArticleDownloader cut;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private Document document;


  @Nested
  class GivenSingleDocument {
    @BeforeEach
    void setUp() {
      given(httpSource.getFrom("myUrl")).willReturn(document);
      given(document.title()).willReturn("Title");
      given(document.html()).willReturn("Body");
      given(document.wholeText()).willReturn("wholeText");
    }

    @Test
    void whenFetchingThenReturnExpected() {
      Article article = cut.fetchArticle("myUrl", false);

      assertThat(article.getFeedTitle()).isEqualTo("myUrl - daily article");
      assertThat(article.getLastUpdate()).isNotNull();
      assertThat(article.getTitle()).isEqualTo("Title");
      assertThat(article.getContent()).isEqualTo("wholeText");
      assertThat(article.getHtmlContent()).isEqualTo("Body");
      assertThat(article.getContentLink()).isEqualTo("myUrl");
    }
  }


  @Nested
  class GivenDocumentWithLinklist {
    @BeforeEach
    void setUp() {
      given(httpSource.getFrom(startsWith(BASE_URL)))
          .willReturn(linkList)
          .willReturn(document);
      given(document.title()).willReturn("Title");
      given(document.html()).willReturn("Body");
    }

    @RepeatedTest(10)
    void whenDownloadingRandomLinkThenReturnArticle() {

      Article article = cut.randomLinkFrom(BASE_URL + "/myPath", "");

      assertThat(article.getTitle()).isEqualTo("Title");
      assertThat(article.getHtmlContent()).isEqualTo("Body");
      assertThat(article.getContentLink()).startsWith(BASE_URL + "/link");
      // check logging to see that different articles were returned
    }

    @RepeatedTest(5)
    void whenDownloadingRandomLinkFromIdThenReturnEitherLink4OrLink5() {

      Article article = cut.randomLinkFrom(BASE_URL + "/myPath", "hallo");

      assertThat(article.getTitle()).isEqualTo("Title");
      assertThat(article.getHtmlContent()).isEqualTo("Body");
      assertThat(article.getContentLink()).containsAnyOf(BASE_URL + "/link4", BASE_URL + "/link5");
    }

    @RepeatedTest(5)
    void whenDownloadingRandomLinkFromCssSelectorThenReturnAlwaysLink3() {

      Article article = cut.randomLinkFrom(BASE_URL + "/myPath", "body>div>a:nth-child(3)");

      assertThat(article.getTitle()).isEqualTo("Title");
      assertThat(article.getHtmlContent()).isEqualTo("Body");
      assertThat(article.getContentLink()).isEqualTo(BASE_URL + "/link3");
    }

    @Test
    void whenDownloadingRandomLinkWithNonExistingSelectorThenReturnAnyArticle() {

      Article article = cut.randomLinkFrom(BASE_URL + "/myPath", "otto");

      assertThat(article.getTitle()).isEqualTo("Title");
      assertThat(article.getHtmlContent()).isEqualTo("Body");
      assertThat(article.getContentLink()).startsWith(BASE_URL + "/link");
      // check logging to see that warning was logged
    }

  }

  @Nested
  @Disabled
  class DownloadRealDocument {
    @Test
    void downloadDocument() {
      cut = new ArticleDownloader(new HttpSource(new JsoupConnection()));

      Article article = cut.fetchArticle(
          "https://www.dwd.de/DE/leistungen/wetterlagenklassifikation/online_wlkvorhersage.txt?view=nasPublication&nn=16102",
          false);
      System.out.println("article = " + article);
      assertThat(article.getHtmlContent()).isNotEmpty();
    }
  }

  private static String resourceAsString(String resourceName) {
    try {
      ClassPathResource classPathResource = new ClassPathResource(resourceName);
      try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
          classPathResource.getInputStream(), StandardCharsets.UTF_8))) {
        return bufferedReader.lines().collect(Collectors.joining(""));
      }
    } catch (IOException ioe) {
      throw new UncheckedIOException(ioe);
    }
  }

}
