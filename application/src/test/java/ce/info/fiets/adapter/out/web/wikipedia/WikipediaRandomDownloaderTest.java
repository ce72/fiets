package ce.info.fiets.adapter.out.web.wikipedia;

import static org.assertj.core.api.Assertions.assertThat;

import ce.info.fiets.adapter.out.web.infrastructure.HttpSource;
import ce.info.fiets.adapter.out.web.infrastructure.JsoupConnection;
import ce.info.fiets.domain.model.Article;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WikipediaRandomDownloaderTest {

  private WikipediaRandomDownloader cut;

  @BeforeEach
  void setUp() {
    cut = new WikipediaRandomDownloader(new HttpSource(new JsoupConnection()));
  }

  @Test
  void doesFetchRandomArticle() {
    Article article = cut.fetchRandomArticle("de", "Philosophie", 2);

    assertThat(article.getFeedTitle()).isEqualTo("de/Philosophie - daily wikipedia article");
    assertThat(article.getLastUpdate()).isNotNull();
    assertThat(article.getTitle()).contains("Wikipedia");
    assertThat(article.getHtmlContent()).contains("mw-content-text");
    assertThat(article.getContentLink()).contains("https://de.wikipedia.org");
  }

}
