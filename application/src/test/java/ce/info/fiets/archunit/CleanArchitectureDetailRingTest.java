package ce.info.fiets.archunit;

import static com.tngtech.archunit.core.domain.JavaClass.Predicates.resideInAnyPackage;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeArchives;
import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeJars;
import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeTests;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.dependencies.SliceRule;

@AnalyzeClasses(packages = ArchConfig.PROJECT_PACKAGE,
    importOptions = {DoNotIncludeTests.class, DoNotIncludeArchives.class, DoNotIncludeJars.class})
public class CleanArchitectureDetailRingTest {

  @ArchTest
  void should_check_domain_services_free_of_cycles(JavaClasses importedClasses) {
    SliceRule sliceRule = slices().matching(ArchConfig.PROJECT_PACKAGE + ".(**).service..").should().beFreeOfCycles();
    sliceRule.check(importedClasses);
  }

  @ArchTest
  void should_check_domain_service_ring(JavaClasses importedClasses) {
    ArchRule rules = classes()
        .that()
        .resideInAPackage("..domain.service..")
        .should()
        .onlyAccessClassesThat(resideInAnyPackage("..domain..", "..usecase.out..")
            .or(resideInAnyPackage("java..", "org.apache.logging.."))
        );
    rules.check(importedClasses);
  }

  @ArchTest
  void should_check_domain_model_ring(JavaClasses importedClasses) {
    ArchRule rules = classes()
        .that()
        .resideInAPackage("..domain.model..")
        .should()
        .onlyAccessClassesThat(resideInAnyPackage("..domain.model..")
            .or(resideInAnyPackage("java..", "org.apache.logging.."))
        );
    rules.check(importedClasses);
  }

  @ArchTest
  void should_check_adapter_in_ring(JavaClasses importedClasses) {
    ArchRule rules = classes()
        .that()
        .resideInAPackage("..adapter.in..")
        .should()
        .onlyAccessClassesThat(resideInAnyPackage("..adapter.in..", "..usecase.in..", "..domain.model..")
            .or(resideInAnyPackage("java..", "org.apache.logging..",
                "jakarta.validation..", "org.springframework..", "com.rometools.."))
        );
    rules.check(importedClasses);
  }

  @ArchTest
  void should_check_adapter_out_ring(JavaClasses importedClasses) {
    ArchRule rules = classes()
        .that()
        .resideInAPackage("..adapter.out..")
        .should()
        .onlyAccessClassesThat(resideInAnyPackage("..adapter.out..", "..domain.model..")
            .or(resideInAnyPackage("java..", "org.apache.logging..",
                "org.jsoup..", "org.htmlunit..", "javax.net.ssl.."))
        );
    rules.check(importedClasses);
  }

  @ArchTest
  void should_check_adapters_not_depend(JavaClasses importedClasses) {
    SliceRule sliceRule = slices().matching(ArchConfig.PROJECT_PACKAGE + ".adapter.(*)..")
        .should().notDependOnEachOther();
    sliceRule.check(importedClasses);
  }

  @ArchTest
  void should_check_usecase_in_ring(JavaClasses importedClasses) {
    ArchRule rules = classes()
        .that()
        .resideInAPackage("..usecase.in..")
        .should()
        .onlyAccessClassesThat(resideInAnyPackage("..domain.model..")
            .or(resideInAnyPackage("java.."))
        );
    rules.check(importedClasses);
  }

  @ArchTest
  void should_check_usecase_out_ring(JavaClasses importedClasses) {
    ArchRule rules = classes()
        .that()
        .resideInAPackage("..usecase.out..")
        .should()
        .onlyAccessClassesThat(resideInAnyPackage("..domain.model..")
            .or(resideInAnyPackage("java.."))
        );
    rules.check(importedClasses);
  }
}
