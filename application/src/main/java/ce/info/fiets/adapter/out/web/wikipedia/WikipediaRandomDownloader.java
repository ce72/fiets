package ce.info.fiets.adapter.out.web.wikipedia;

import ce.info.fiets.adapter.out.web.infrastructure.HttpSource;
import ce.info.fiets.domain.model.Article;

import java.time.Instant;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

@Component
@Log4j2
@RequiredArgsConstructor
public class WikipediaRandomDownloader implements ce.info.fiets.usecase.out.FetchWikipediaArticle {
  private static final Pattern REFRESH_URL_PATTERN = Pattern.compile("(?si)\\d+;\\s*url=(.+)|\\d+");

  private static final String RANDOM_ARTICLE_URL_TEMPLATE =
      "https://tools.wmflabs.org/magnustools/randomarticle.php?lang=%s&project=wikipedia&categories=%s&d=%s";
  private static final String MW_CONTENT_TEXT = "mw-content-text";

  @NonNull
  private final HttpSource httpSource;

  @Override
  public Article fetchRandomArticle(String language, String category, int depth) {
    String url = selectRandomArticle(language, category, depth);

    PageContent pageContent = fetchArticle(url + "?printable=yes");

    Instant now = Instant.now();
    return Article.builder()
        .feedTitle(language + "/" + category + " - daily wikipedia article")
        .lastUpdate(now)
        .lastCheckDate(now)
        .title(pageContent.getTitle())
        .htmlContent(pageContent.getContent())
        .contentLink(url)
        .build();
  }

  private PageContent fetchArticle(String url) {
    log.info("Read article from " + url);

    Document document = httpSource.getFrom(url);
    String content = Optional.ofNullable(document.body().getElementById(MW_CONTENT_TEXT))
        .map(Node::outerHtml)
        .orElse("no such id in body: " + MW_CONTENT_TEXT);
    return PageContent.builder()
        .title(document.title())
        .content(content)
        .build();
  }

  private String selectRandomArticle(String language, String category, int depth) {
    String categoryUrl = String.format(RANDOM_ARTICLE_URL_TEMPLATE, language, category, depth);

    log.info("Select random article from " + categoryUrl);
    Document document = httpSource.getFrom(categoryUrl);

    Elements refreshMetaTag = document.select("html head meta[http-equiv=refresh]");

    Matcher matcher = REFRESH_URL_PATTERN.matcher(refreshMetaTag.attr("content"));

    if (matcher.matches() && matcher.group(1) != null) {
      return "https:" + matcher.group(1);
    } else {
      throw new IllegalArgumentException("No Article found for Category: " + language + "/" + category);
    }
  }
}
