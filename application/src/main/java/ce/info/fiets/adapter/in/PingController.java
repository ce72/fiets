package ce.info.fiets.adapter.in;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class PingController {

  @GetMapping(path = "ping")
  public String get() {
    return "Ping success";
  }
}
