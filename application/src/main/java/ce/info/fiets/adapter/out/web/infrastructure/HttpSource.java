package ce.info.fiets.adapter.out.web.infrastructure;

import static java.text.MessageFormat.format;

import ce.info.fiets.domain.model.RemoteException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.htmlunit.WebClient;
import org.htmlunit.html.HtmlPage;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class HttpSource {

  @NonNull
  private final JsoupConnection jsoupConnection;

  public Document getFrom(String url) {
    try {
      return jsoupConnection.get(url);
    } catch (SocketTimeoutException tex) {
      throw new RemoteException(format("Timeout while getting url {0}: {1}",
          url, tex.toString()));
    } catch (HttpStatusException hex) {
      throw new RemoteException(format("HTTP status {0} while getting url {1}",
          hex.getStatusCode(), url));
    } catch (MalformedURLException | IllegalArgumentException iaex) {
      throw new IllegalArgumentException(format("Malformed url while getting url {0}: {1}",
          url, iaex.toString()));
    } catch (IOException | RuntimeException ioex) {
      throw new RemoteException(format("Failed to get url {0}: {1}",
          url, ioex.toString()),
          ioex);
    }
  }

  public Document getScriptedFrom(String url) {
    try (WebClient webClient = new WebClient()) {
      webClient.getOptions().setThrowExceptionOnScriptError(false);
      webClient.getOptions().setUseInsecureSSL(true);
      HtmlPage myPage = webClient.getPage(url);
      webClient.waitForBackgroundJavaScript(10_000);
      return Jsoup.parse(myPage.asXml());
    } catch (MalformedURLException | IllegalArgumentException iaex) {
      throw new IllegalArgumentException(format("Malformed url while getting url {0}: {1}",
          url, iaex.toString()));
    } catch (IOException | RuntimeException ioex) {
      throw new RemoteException(format("Failed to get url {0}: {1}",
          url, ioex.toString()),
          ioex);
    }
  }

}
