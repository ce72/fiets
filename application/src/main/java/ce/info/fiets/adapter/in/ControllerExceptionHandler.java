package ce.info.fiets.adapter.in;

import ce.info.fiets.domain.model.RemoteException;

import jakarta.validation.ConstraintViolationException;
import java.util.Map;
import java.util.Objects;
import lombok.extern.log4j.Log4j2;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.resource.NoResourceFoundException;

@ControllerAdvice
@Log4j2
public class ControllerExceptionHandler {
  @ExceptionHandler(ClientAbortException.class)
  public void handle(ClientAbortException exception) {
    log.info(exception::toString);
  }

  @ExceptionHandler(NoResourceFoundException.class)
  public void handle(NoResourceFoundException exception) {
    log.info(exception::toString);
  }

  @ExceptionHandler(MissingServletRequestParameterException.class)
  public void handle(MissingServletRequestParameterException exception) {
    log.info("!!! MissingServletRequestParameterException {}", exception.getMessage());
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<Map<String, String>> handleConstraintViolationException(ConstraintViolationException cex) {
    return toWarningResponse(cex);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<Map<String, String>> toResponse(IllegalArgumentException exception) {
    return toWarningResponse(exception);
  }

  @ExceptionHandler(HttpMessageConversionException.class)
  public ResponseEntity<Map<String, String>> toResponse(HttpMessageConversionException exception) {
    return toWarningResponse(exception);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Map<String, String>> toResponse(MethodArgumentNotValidException exception) {
    return toWarningResponse(exception);
  }

  @ExceptionHandler(ServletRequestBindingException.class)
  public ResponseEntity<Map<String, String>> toResponse(ServletRequestBindingException exception) {
    return toWarningResponse(exception);
  }

  @ExceptionHandler(RemoteException.class)
  public ResponseEntity<String> handleRemoteException(RemoteException cex) {
    if (Objects.isNull(cex.getCause())) {
      log.warn(cex.getMessage());
    } else {
      log.error(cex.getMessage(), cex.getCause());
    }
    return new ResponseEntity<>(cex.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Map<String, String>> toErrorResponse(Exception exception) {
    log.error(exception, exception);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .contentType(MediaType.APPLICATION_JSON)
        .body(Map.of(
            "status", "ERROR",
            "message", exception.toString()));
  }

  private ResponseEntity<Map<String, String>> toWarningResponse(Exception exception) {
    log.warn(exception);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .contentType(MediaType.APPLICATION_JSON)
        .body(Map.of(
            "status", "WARN",
            "message", exception.getMessage()));
  }

}
