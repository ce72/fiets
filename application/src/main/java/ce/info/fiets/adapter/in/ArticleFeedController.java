package ce.info.fiets.adapter.in;

import ce.info.fiets.domain.model.Article;
import ce.info.fiets.usecase.in.ClearDailyArticles;
import ce.info.fiets.usecase.in.GetDailyArticle;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import java.time.LocalTime;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.View;

@RestController
@RequiredArgsConstructor
@RequestMapping("api")
@Validated
public class ArticleFeedController {

  @NonNull
  private final GetDailyArticle articleSource;

  @NonNull
  private final ClearDailyArticles clearDailyArticles;

  @GetMapping(path = "wikipedia/{language}/{category}", produces = "application/atom+xml")
  public View getWikipedia(
      @PathVariable("language")
      @Size(min = 1, max = 5)
      String language, // size< 5
      @PathVariable("category")
      @Size(max = 100) @NotBlank
      String category, // size< 100
      @RequestParam(name = "updateTime", defaultValue = "05:00")
      @Pattern(regexp = "[0-2]\\d:[0-5]\\d")
      String updateTime,
      @RequestParam(name = "depth", defaultValue = "5")
      @Max(50)
      int depth
  ) {
    LocalTime requestedUpdateTime = LocalTime.parse(updateTime);
    Article article = articleSource.randomWikipediaArticleFor(language, category, requestedUpdateTime, depth);
    return new ArticleFeedView(article);
  }

  @GetMapping(path = "article", produces = "application/atom+xml")
  public View getArticle(@RequestParam(name = "updateTime", defaultValue = "05:00")
                         @Pattern(regexp = "[0-2]\\d:[0-5]\\d")
                         String updateTime,
                         @RequestParam(name = "allowScripting", defaultValue = "false")
                         boolean allowScripting,
                         @RequestParam(name = "alwaysRefresh", defaultValue = "false")
                         boolean alwaysRefresh,
                         @RequestParam(name = "contentUrl")
                         @Size(min = 5, max = 256)
                         String contentUrl) {
    LocalTime requestedUpdateTime = LocalTime.parse(updateTime);
    Article article = articleSource.sameArticleFrom(contentUrl, requestedUpdateTime, allowScripting, alwaysRefresh);
    return new ArticleFeedView(article);
  }

  @GetMapping(path = "linkFrom", produces = "application/atom+xml")
  public View getLinkFrom(
      @RequestParam(name = "updateTime", defaultValue = "05:00")
      @Pattern(regexp = "[0-2]\\d:[0-5]\\d")
      String updateTime,
      @RequestParam(name = "url")
      @Size(min = 5, max = 256)
      String url,
      @RequestParam(name = "cssid", defaultValue = "")
      @Size(max = 256)
      String cssId
  ) {
    LocalTime requestedUpdateTime = LocalTime.parse(updateTime);
    String clearedCssId = cssId.replace("\"", "");
    Article article = articleSource.randomLinkFrom(url, clearedCssId, requestedUpdateTime);
    return new ArticleFeedView(article);
  }

  @GetMapping(path = "proxy", produces = MediaType.TEXT_PLAIN_VALUE)
  public String getProxy(
      @RequestParam(name = "url")
      @Size(min = 5, max = 256)
      String url
  ) {
    Article article = articleSource.alwaysDownload(url);
    return article.getContent();
  }

  @PostMapping(path = "reset")
  public void reset() {
    clearDailyArticles.remove();
  }
}
