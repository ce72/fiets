package ce.info.fiets.adapter.out.web.infrastructure;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

@Component
public class JsoupConnection {
  public Document get(String url) throws IOException {
    return Jsoup.connect(url)
        .sslSocketFactory(socketFactory())
        .timeout(60_000)
        .maxBodySize(0)
        .get();
  }

  private static SSLSocketFactory socketFactory() {
    // https://stackoverflow.com/questions/40742380/how-to-resolve-jsoup-error-unable-to-find-valid-certification-path-to-requested/40742828#answer-58325184
    TrustManager[] trustAllCerts = {new X509TrustManager() {

      @Override
      public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
      }

      @Override
      @SuppressWarnings("java:S4830") // we are disabling cert validation here
      public void checkClientTrusted(X509Certificate[] certs, String authType) {
        // trust em
      }

      @Override
      @SuppressWarnings("java:S4830") // we are disabling cert validation here
      public void checkServerTrusted(X509Certificate[] certs, String authType) {
        // trust em
      }
    }
    };

    try {
      SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
      sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

      return sslContext.getSocketFactory();
    } catch (NoSuchAlgorithmException | KeyManagementException securityException) {
      throw new IllegalStateException("Failed to create a SSL socket factory", securityException);
    }
  }
}
