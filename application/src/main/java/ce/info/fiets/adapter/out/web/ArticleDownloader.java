package ce.info.fiets.adapter.out.web;

import static java.util.function.Predicate.not;

import ce.info.fiets.adapter.out.web.infrastructure.HttpSource;
import ce.info.fiets.domain.model.Article;
import ce.info.fiets.usecase.out.FetchArticle;

import java.security.SecureRandom;
import java.time.Instant;
import java.util.List;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Collector;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ArticleDownloader implements FetchArticle {

  private final HttpSource httpSource;

  private final SecureRandom random;

  public ArticleDownloader(@NonNull HttpSource httpSource) {
    this.httpSource = httpSource;
    this.random = new SecureRandom();
  }

  @Override
  public Article fetchArticle(String contentUrl, boolean allowScripting) {
    log.info("Read article from {}", contentUrl);

    Document document = allowScripting ? httpSource.getScriptedFrom(contentUrl) : httpSource.getFrom(contentUrl);
    Instant now = Instant.now();
    return Article.builder()
        .feedTitle(contentUrl + " - daily article")
        .lastCheckDate(now)
        .lastUpdate(now)
        .title(document.title())
        .content(document.wholeText())
        .htmlContent(document.html())
        .contentLink(contentUrl)
        .build();
  }

  @Override
  public Article randomLinkFrom(String linkArticleUrl, String cssId) {
    log.info("Fetch link from {}", linkArticleUrl);
    Element startPage = httpSource.getFrom(linkArticleUrl);

    Elements linkArea = selectArea(startPage, cssId);

    Elements links = linkArea.select("a[href]");
    List<String> targetUrls = links.stream()
        .map(l -> l.attr("abs:href"))
        .filter(not(String::isEmpty))
        .toList();

    log.info("Select one article from {} links", targetUrls::size);

    String targetUrl = targetUrls.get(random.nextInt(targetUrls.size()));
    return fetchArticle(targetUrl, false);
  }

  private Elements selectArea(Element startPage, String cssId) {
    if (cssId.isBlank()) {
      return new Elements(startPage);
    } else {
      Elements area = Collector.collect(new Evaluator.Id(cssId), startPage);
      if (area.isEmpty()) {
        area = startPage.select(cssId);
        if (area.isEmpty()) {
          log.warn("No element found for {}", cssId);
          area = new Elements(startPage);
        }
      }
      return area;
    }
  }
}
