package ce.info.fiets.adapter.in;

import ce.info.fiets.domain.model.Article;

import com.rometools.rome.feed.atom.Content;
import com.rometools.rome.feed.atom.Entry;
import com.rometools.rome.feed.atom.Feed;
import com.rometools.rome.feed.atom.Link;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.view.feed.AbstractAtomFeedView;

public class ArticleFeedView extends AbstractAtomFeedView {
  private final Article article;

  public ArticleFeedView(Article article) {
    super();
    this.article = article;
  }

  @Override
  protected void buildFeedMetadata(Map<String, Object> model,
                                   Feed feed,
                                   HttpServletRequest request) {
    feed.setTitle(article.getFeedTitle());
    feed.setModified(Date.from(article.getLastUpdate()));
  }

  @Override
  protected List<Entry> buildFeedEntries(Map<String, Object> map,
                                         HttpServletRequest httpServletRequest,
                                         HttpServletResponse httpServletResponse) {
    Entry entry = new Entry();

    Date articleUpdateTime = Date.from(article.getLastUpdate());
    entry.setUpdated(articleUpdateTime);
    entry.setCreated(articleUpdateTime);
    entry.setIssued(articleUpdateTime);
    entry.setModified(articleUpdateTime);

    entry.setTitle(article.getTitle());

    Link link = new Link();
    link.setHref(article.getContentLink());
    link.setTitle(article.getTitle());
    entry.setAlternateLinks(List.of(link));

    Content content = new Content();
    content.setType(MediaType.TEXT_HTML_VALUE);
    content.setValue(article.getHtmlContent());
    entry.setContents(List.of(content));

    entry.setId(article.getTitle() + articleUpdateTime.getTime());
    return List.of(entry);
  }
}
