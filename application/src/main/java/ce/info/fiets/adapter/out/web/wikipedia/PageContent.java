package ce.info.fiets.adapter.out.web.wikipedia;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PageContent {
  String title;
  String content;
}
