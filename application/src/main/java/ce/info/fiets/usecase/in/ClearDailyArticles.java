package ce.info.fiets.usecase.in;

public interface ClearDailyArticles {
  void remove();
}
