package ce.info.fiets.usecase.out;

import ce.info.fiets.domain.model.Article;

public interface FetchWikipediaArticle {
  Article fetchRandomArticle(String language, String category, int depth);
}
