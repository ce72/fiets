package ce.info.fiets.usecase.in;

import ce.info.fiets.domain.model.Article;

import java.time.LocalTime;

public interface GetDailyArticle {
  Article randomWikipediaArticleFor(String language, String category, LocalTime updateTimeUtc, int depth);

  Article sameArticleFrom(String contentUrl, LocalTime updateTimeUtc, boolean allowScripting, boolean alwaysRefresh);

  Article randomLinkFrom(String url, String cssId, LocalTime updateTimeUtc);

  Article alwaysDownload(String url);
}
