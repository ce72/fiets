package ce.info.fiets.usecase.out;

import ce.info.fiets.domain.model.Article;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends CrudRepository<Article, String> {

  Optional<Article> findByKey(String key);
}
