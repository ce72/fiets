package ce.info.fiets.usecase.out;

import ce.info.fiets.domain.model.Article;

public interface FetchArticle {
  Article fetchArticle(String contentUrl, boolean allowScripting);

  Article randomLinkFrom(String linkArticleUrl, String cssId);
}
