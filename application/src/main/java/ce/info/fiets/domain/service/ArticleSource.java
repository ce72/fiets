package ce.info.fiets.domain.service;

import ce.info.fiets.domain.model.Article;
import ce.info.fiets.usecase.in.ClearDailyArticles;
import ce.info.fiets.usecase.in.GetDailyArticle;
import ce.info.fiets.usecase.out.ArticleRepository;
import ce.info.fiets.usecase.out.FetchArticle;
import ce.info.fiets.usecase.out.FetchWikipediaArticle;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Supplier;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class ArticleSource implements GetDailyArticle, ClearDailyArticles {

  @NonNull
  private final FetchWikipediaArticle wikipediaRandomDownloader;

  @NonNull
  private final FetchArticle articleDownloader;

  @NonNull
  private final ArticleRepository articles;

  @Override
  public Article randomWikipediaArticleFor(String language, String category, LocalTime updateTime, int depth) {
    String key = language + category + "_" + updateTime.format(DateTimeFormatter.ISO_LOCAL_TIME);
    Supplier<Article> articleSupplier =
        () -> wikipediaRandomDownloader.fetchRandomArticle(language, category, depth).setKey(key);

    return getAndCache(updateTime, key, articleSupplier);
  }

  @Override
  public Article sameArticleFrom(String contentUrl, LocalTime updateTime,
                                 boolean allowScripting, boolean alwaysRefresh) {
    String key = contentUrl + "_" + updateTime.format(DateTimeFormatter.ISO_LOCAL_TIME);
    Supplier<Article> articleSupplier =
        () -> articleDownloader.fetchArticle(contentUrl, allowScripting).setKey(key);

    return getAndCache(updateTime, key, articleSupplier, alwaysRefresh);
  }

  @Override
  public Article randomLinkFrom(String url, String cssId, LocalTime updateTime) {
    String key = url + "_" + cssId + "_" + updateTime.format(DateTimeFormatter.ISO_LOCAL_TIME);
    Supplier<Article> articleSupplier =
        () -> articleDownloader.randomLinkFrom(url, cssId).setKey(key);

    return getAndCache(updateTime, key, articleSupplier);
  }

  @Override
  public Article alwaysDownload(String url) {
    return articleDownloader.fetchArticle(url, false);
  }

  @Override
  public void remove() {
    log.info("Delete all from cache");
    articles.deleteAll();
  }

  private Article getAndCache(LocalTime updateTime, String key, Supplier<Article> articleSupplier) {
    return getAndCache(updateTime, key, articleSupplier, false);
  }

  private Article getAndCache(LocalTime updateTime, String key, Supplier<Article> articleSupplier,
                              boolean alwaysRefresh) {
    log.info("Article requested for Key: {}", key);
    Article result = articles.findByKey(key)
        .map(storedArticle -> updateWhenExpiredAndDifferent(storedArticle, updateTime, articleSupplier,
            alwaysRefresh))
        .orElseGet(() -> articles.save(articleSupplier.get()));
    log.info("Article [{}] returned with lastUpdate={}",
        result.getTitle(),
        DateTimeFormatter.ISO_INSTANT.format(result.getLastUpdate()));
    return result;
  }

  private Article updateWhenExpiredAndDifferent(Article storedArticle, LocalTime updateTime,
                                                Supplier<Article> articleSupplier, boolean alwaysRefresh) {
    ZonedDateTime now = ZonedDateTime.now(ZoneId.systemDefault());
    if (storedArticle.isExpired(now, updateTime)) {
      Article newArticle = articleSupplier.get(); // this loads the article
      if (!alwaysRefresh && storedArticle.getHtmlContent().equals(newArticle.getHtmlContent())) {
        // keep old content in DB because it has not changed
        storedArticle.setLastCheckDate(Instant.from(now));
      } else {
        storedArticle.setContentLink(newArticle.getContentLink());
        storedArticle.setFeedTitle(newArticle.getFeedTitle());
        storedArticle.setLastCheckDate(Instant.from(now));
        storedArticle.setLastUpdate(newArticle.getLastUpdate());
        storedArticle.setTitle(newArticle.getTitle());
        storedArticle.setContent(newArticle.getContent());
        storedArticle.setHtmlContent(newArticle.getHtmlContent());
      }
      return articles.save(storedArticle);
    }
    return storedArticle;
  }

}
