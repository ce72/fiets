package ce.info.fiets.domain.model;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder(toBuilder = true)
@Document(collection = "articles")
public class Article {
  @Id
  String id;

  @Accessors(chain = true)
  String key;
  String title;
  String content;
  String htmlContent;
  String contentLink;
  String feedTitle;
  @EqualsAndHashCode.Exclude
  Instant lastUpdate;
  @EqualsAndHashCode.Exclude
  Instant lastCheckDate;

  public boolean isExpired(ZonedDateTime now, LocalTime updateTime) {
    ZonedDateTime todayAtUpdateTime = now
        .withHour(updateTime.getHour())
        .withMinute(updateTime.getMinute())
        .truncatedTo(ChronoUnit.MINUTES);
    ZonedDateTime yesterdayAtUpdateTime = todayAtUpdateTime
        .minusDays(1)
        .truncatedTo(ChronoUnit.MINUTES);

    // stored lastCheckDate < jetzt-24h
    // or ((updateTime is later than now) and (has not been updated today))
    return lastCheckDate.isBefore(yesterdayAtUpdateTime.toInstant())
        || now.isAfter(todayAtUpdateTime) && !lastCheckDate.isAfter(todayAtUpdateTime.toInstant());
  }

}
