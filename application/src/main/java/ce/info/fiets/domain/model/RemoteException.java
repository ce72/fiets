package ce.info.fiets.domain.model;

import java.io.Serial;

public class RemoteException extends RuntimeException {
  @Serial
  private static final long serialVersionUID = -1L;

  public RemoteException(String message) {
    super(message);
  }

  public RemoteException(String message, Throwable cause) {
    super(message, cause);
  }
}
