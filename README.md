# fiets ![rss](https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Feed-icon.svg/128px-Feed-icon.svg.png)

Provide [web feeds](https://en.wikipedia.org/wiki/Web_feed) with daily changing, random content

[![pipeline status](https://gitlab.com/ce72/fiets/badges/master/pipeline.svg)](https://gitlab.com/ce72/fiets/-/commits/master)
[![coverage report](https://gitlab.com/ce72/fiets/badges/master/coverage.svg)](https://gitlab.com/ce72/fiets/commits/master)
[![Release](https://gitlab.com/ce72/fiets/-/badges/release.svg)](https://gitlab.com/ce72/fiets/-/releases)

## How to build

```shell
mvn clean install
```

or, to run integration-tests:

```shell
mvn clean install -Pintegration-test
```

### Run local docker container

```shell
docker-compose up
```

Launches both the service and a mongodb.

### Alternative: simply pull the image from gitlab registry

No need to build:

```shell
docker run --name fiets -d -p 8080:8080 registry.gitlab.com/ce72/fiets:latest
```

## How to use

(see also `http://localhost:8080/readme.html`)

### Random wikipedia article from category tree

Select a new random wikipedia article of the category 'Plants' (and up to 10 subcategories), every day at 04:00 UTC

```shell
http://localhost:8080/api/wikipedia/en/Plants?updateTime=04:00&depth=2
```

Then import into your [favourite](https://www.inoreader.com/) feed reader!

#### Description

The general format of the url is

```shell
/api/wikipedia/{language}/{category}

/api/wikipedia/{language}/{category}?updateTime={updateTime}}&depth={depth}
```

where

| Parameter  | Description                                                                         |
| ---------- | ----------------------------------------------------------------------------------- |
| language   | wikipedia language path parameter. eg 'en', 'de'                                    |
| category   | top article category path parameter to select from                                  |
| updateTime | optional request parameter (default=09:00 UTC)                                      |
| depth      | optional request parameter number of sub category hierarchies to follow (default=5) |

### Daily copy of same url

Create a news feed once a day with the content of the given url

```shell
http://localhost:8080/api/article?updateTime=18:00&contentUrl=http://www.tagesschau.de
```

where

| Parameter  | Description                                    |
| ---------- | ---------------------------------------------- |
| updateTime | optional request parameter (default=09:00 UTC) |
| contentUrl | target url                                     |

### Daily random link from page

Create a news feed with a randomly selected link once per day from the given html page

```shell
http://localhost:8080/api/linkFrom?updateTime={hh:mm}&url={url}&cssid={cssid}
```
where

| Parameter  | Description                                                                  |
| ---------- | ---------------------------------------------------------------------------- |
| url        | target url                                                                   |
| updateTime | optional request parameter (default=09:00 UTC)                               |
| cssid      | optional id of the sub node to scan links from (default: use the whole site) |
