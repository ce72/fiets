Feature: Tests for validation and error handling

  Scenario Outline: Bad requests are answered with Status 400 and message
    When I get "<url>" from service
    Then I receive a response with status <status>
    And the response contains the text "<message>"

    Examples:
      | url                                                               | message                                                | status |
      | /api/wikipedia/d12345/Botanik                                     | getWikipedia.language: size must be between 1 and 5    | 400    |
      | /api/wikipedia/de/Botanik?updateTime=44:96&depth=500              | getWikipedia.updateTime: must match                    | 400    |
      | /api/wikipedia/de/Botanik?updateTime=44:96&depth=500              | getWikipedia.depth: must be less than or equal to 50   | 400    |
      | /api/article?updateTime=44:96&contentUrl=xx                       | getArticle.updateTime: must match                      | 400    |
      | /api/article?updateTime=44:96&contentUrl=xx                       | getArticle.contentUrl: size must be between 5 and 256  | 400    |
      | /api/linkFrom?updateTime=11:44&url=xx                             | getLinkFrom.url: size must be between 5 and 256        | 400    |
      # Remote errors
      | /api/linkFrom?updateTime=11:44&url=http://asdasdasd.sadasd.sadasd | java.net.UnknownHostException: asdasdasd.sadasd.sadasd | 503    |
      | /api/linkFrom?updateTime=11:44&url=httxxxxp://a.b.c               | Malformed url                                          | 400    |
