Feature: Returns various blog feeds -- assuming that wikipedia and www.example.com can be reached
# calls real urls, no wiremock

  Scenario: Download random wikipedia article and wrap into atom feed
    When I get a random wikipedia article for language "de" with category "Philosophie"
    Then I receive a response with status 200
     And I receive a news feed with title "de/Philosophie - daily wikipedia article"
     And there will be one wikipedia article
    When I get a random wikipedia article for language "de" with category "Philosophie"
    Then I receive the same answer

    When I get "/dapi/articles" from service
    Then I receive a response with status 200


  Scenario: Download same article from url and wrap into atom feed
    When I get an article from url "http://www.example.com"
    Then I receive a response with status 200
     And I receive a news feed with title "http://www.example.com - daily article"
    When I get an article from url "http://www.example.com"
    Then I receive the same answer

  Scenario: Download any article from link list and wrap into atom feed
    When I get a random link from url "https://de.wikipedia.org/wiki/Spezial:Alle_Seiten"
    Then I receive a response with status 200
     And the response contains the text " - daily article"
    When I get a random link from url "https://de.wikipedia.org/wiki/Spezial:Alle_Seiten"
    Then I receive the same answer

  Scenario: Proxy content
    When I get "/api/proxy?url=http://www.example.com" from service
    Then I receive a response with status 200
    And the response contains the text "Example Domain"
