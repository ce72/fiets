package ce.integration;

import ce.integration.api.RequestResponseDump;
import ce.integration.api.ResponseWorld;
import ce.integration.util.JsonPathAssertions;

import io.cucumber.java.Before;
import io.cucumber.spring.CucumberContextConfiguration;
import io.restassured.RestAssured;
import org.springframework.boot.test.context.SpringBootTest;

@CucumberContextConfiguration
@SpringBootTest(classes = {ResponseWorld.class, RequestResponseDump.class, JsonPathAssertions.class})
public class TestSetup {

  public static String getServiceHost() {
    return System.getProperty("service.host", "localhost");
  }

  public static int getServicePort() {
    return Integer.parseInt(System.getProperty("service.port", "8080"));
  }

  public static int getActuatorPort() {
    return Integer.parseInt(System.getProperty("service.actuator.port", "8081"));
  }

  public static String getServiceUrl() {
    return "http://" + getServiceHost();
  }


  @Before
  public void configureRestAssured() {
    RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    RestAssured.baseURI = getServiceUrl();
    RestAssured.port = getServicePort();
  }

}
