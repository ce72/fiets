package ce.integration.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import org.springframework.core.io.ClassPathResource;

public class ResourceUtils {
  public static String resourceAsString(String resourceName) {
    try {
      ClassPathResource classPathResource = new ClassPathResource(resourceName);
      try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
          classPathResource.getInputStream(), StandardCharsets.UTF_8))) {
        return bufferedReader.lines().collect(Collectors.joining("\n"));
      }
    } catch (IOException ioe) {
      throw new UncheckedIOException(ioe);
    }
  }
}
