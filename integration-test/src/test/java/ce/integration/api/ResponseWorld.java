package ce.integration.api;

import io.cucumber.spring.ScenarioScope;
import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import java.util.ArrayList;
import java.util.List;

@ScenarioScope
public class ResponseWorld {
  private final List<Response> responses = new ArrayList<>();

  public void set(Response response) {
    this.responses.add(response);
  }

  public Filter set() {
    return new ResponseWorldFilter(this);
  }

  public Response get() {
    return responses.get(responses.size() - 1);
  }

  public Response getPrevious() {
    return responses.get(responses.size() - 2);
  }

  private record ResponseWorldFilter(ResponseWorld responseWorld) implements Filter {
    @Override
    public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec,
                           FilterContext ctx) {
      Response response = ctx.next(requestSpec, responseSpec);
      responseWorld.set(response);
      return response;
    }
  }
}
