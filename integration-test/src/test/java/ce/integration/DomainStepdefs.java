package ce.integration;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import ce.integration.api.RequestResponseDump;
import ce.integration.api.ResponseWorld;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DomainStepdefs {
  private final ResponseWorld responseWorld;
  private final RequestResponseDump requestResponseDump;

  public DomainStepdefs(ResponseWorld responseWorld, RequestResponseDump requestResponseDump) {
    this.responseWorld = responseWorld;
    this.requestResponseDump = requestResponseDump;
  }

  @When("I get a random wikipedia article for language {string} with category {string}")
  public void getWikipediaArticle(String language, String category) {
    String uri = String.format("/api/wikipedia/%s/%s", language, category);

    getAtomFeedFrom(uri);
  }

  @When("I get an article from url {string}")
  public void getArticleFromUrl(String contentUrl) {
    String uri = String.format("/api/article?contentUrl=%s&updateTime=04:00", contentUrl);
    getAtomFeedFrom(uri);
  }

  @When("I get a random link from url {string}")
  public void getRandomLinkFromUrl(String url) {
    String uri = String.format("/api/linkFrom?url=%s", url);
    getAtomFeedFrom(uri);
  }

  @Then("I receive a news feed with title {string}")
  public void newsFeedWithTitle(String title) {
    responseWorld.get().then()
        .assertThat().body("feed.title", is(equalTo(title)))
        .and().body("feed.entry.id", not(blankOrNullString()))
        .and().body("feed.entry.published", not(blankOrNullString()))
        .and().body("feed.entry.link.@href", not(blankOrNullString()))
        .and().body("feed.entry.link.@title", not(blankOrNullString()))
        .and().body("feed.entry.content", is(not(blankOrNullString())));
  }

  @Then("there will be one wikipedia article")
  public void thereWillBeOneWikipediarticle() {
    responseWorld.get().then()
        .assertThat().body("feed.entry.title", containsString("Wikipedia"))
        .and().body("feed.entry.id", not(blankOrNullString()))
        .and().body("feed.entry.published", not(blankOrNullString()))
        .and().body("feed.entry.link.@href", containsString("wikipedia.org"))
        .and().body("feed.entry.link.@title", containsString("Wikipedia"))
        .and().body("feed.entry.content", containsString("mw-content-text"));
  }

  private void getAtomFeedFrom(String uri) {
    given()
        .filter(responseWorld.set())
        .filter(requestResponseDump.dumpResponseFilter(uri))
        .baseUri(TestSetup.getServiceUrl())
        .port(TestSetup.getServicePort())
        .accept("application/atom+xml")
        .log().ifValidationFails()
        .when()
        .get(uri)
        .then()
        .extract().response();
  }

  @Then("I receive the same answer")
  public void receiveTheSameAnswer() {
    assertThat(responseWorld.get().asString(), is(equalTo(responseWorld.getPrevious().asString())));
  }
}
